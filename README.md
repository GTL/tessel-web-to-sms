# README #


### What is this repository for? ###

The project is a demonstration of the Tessel and its GPRS module.

A node.js server connects to web and accepts request.  These request are forwarded to a Tessel running on local network via RESTful API calls.  The Tessel will then send SMS message defined by the URL.

The Tessel server also has the ability to take photos with attached camera module and pass back, as well as log request onto MicroSD module.

### Demo ###
You can access a live demo of the code at: http://tessel.gametheorylabs.com:8080/sms/11DigitPhoneNumber/messageToSend
Note: I am actively developing with Tessel, so this link may be down and change functionality during my development sessions.

### How do I get set up? ###

Update the IP address for Tessel in server.js
Tessel does not have to be accessible from internet, if NodeJS server and Tessel are on the same network.
If you have questions, contact me on Twitter @CoreyClarkPhD

