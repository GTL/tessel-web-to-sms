//webserver modified version from github https://github.com/dwhitnee/tessel

var port = 8080;
var tesselAPIServerIP = '192.168.1.3';
var http = require('http');      // the interwebs
url = require('url');
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/'));
app.use('/sms', function(req, res){
  console.log("SMS request from: " + req.connection.remoteAddress);
  res.setHeader('Connection', 'Transfer-Encoding');
  res.setHeader('Content-Type', 'text/html; charset=utf-8');
  res.setHeader('Transfer-Encoding', 'chunked');
  RequestSMS(req, res);

});
app.use('/snapshot', function(req, res){
  console.log("Picture request from: " + req.connection.remoteAddress);
    res.setHeader('Connection', 'Transfer-Encoding');
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.setHeader('Transfer-Encoding', 'chunked');
    RequestPicture(res);

});

app.listen(process.env.PORT || port);
console.log("listening on port " + port);

var RequestSMS = function(request, response){
  // Set up a request
  var req = http.request({
    port: 8000,
    method: 'GET',
    hostname: tesselAPIServerIP,
    path: request.originalUrl,
    headers: {
      Host: tesselAPIServerIP,
      'Accept': '*/*',
      "User-Agent": "tessel",
      'Content-Type': 'text/html; charset=utf-8',
      'Connection': 'keep-alive'
    }
  }, function (res) {
    res.on('data', function(tesselReply) {
      Responses.sms( response );
      console.log('    Tessel Replied: ' + tesselReply);
    });
  });
  console.log("    Calling Tessel");
  req.end();

  // Log any errors
  req.on('error', function(e) {
    console.error(e);
  });
}
var RequestPicture = function( response ){
  // Set up a request
  var req = http.request({
    port: 8000,
    method: 'GET',
    hostname: tesselAPIServerIP,
    path: '/snapshot',
    headers: {
      Host: tesselAPIServerIP,
      'Accept': '*/*',
      "User-Agent": "tessel",
      'Content-Type': 'text/html; charset=utf-8',
      'Connection': 'keep-alive'
    }
  }, function (res) {
    res.on('data', function(filename) {
      Responses.snapshot( response, filename );
      console.log('    Tessel Replied: ' + filename);
    });
  });
  console.log("    Calling Tessel");
  req.end();

  // Log any errors
  req.on('error', function(e) {
    console.error(e);
  });
}




// //----------------------------------------
var Responses = {
  snapshot: function( response, filename) {
    //Tessel.flash();
    Responses.writeHeader( response, 200 );
    response.write('<div style="padding: 3em">Picture taken and served by Tessel <a href="'+ filename+'">Click Here</a> <br/>(If image does not show up, refresh page to give Tessel time to save image)</div>\n');
    response.write('<div style="padding: 0.5 em">1.  NodeJS server calls RESTful API server on Tessel</div>\n');
    response.write('<div style="padding: 0.5 em">2.  Tessel calls into Camera module, takes a picture and stores on server</div>');
    response.write('<div style="padding: 0.5 em">3.  NodeJS server replies with this custom built page, with link to photo</div>');
    Responses.writeFooter(response);
    
  },
  sms: function( response ){
    Responses.writeHeader( response, 200 );
    response.write('<div style="padding: 3em">The Tessel will be sending your message shortly</div>\n');
    response.write('<div style="padding: 0.5 em">1.  NodeJS server calls RESTful API server on Tessel</div>\n');
    response.write('<div style="padding: 0.5 em">2.  Tessel calls into GPRS module to send SMS</div>');
    response.write('<div style="padding: 0.5 em">3.  NodeJS server replies with this custom built page</div>');
    Responses.writeFooter(response);
    
  },
  writeHeader: function( response, status ) {
    response.writeHead( status );
    response.write("<html><head>\n");
    response.write('<link rel="icon" type="image/png" href="http://start.tessel.io/favicon.ico">\n');
    response.write("<title>Corey's Tessel</title>\n");
    response.write('<body style="font-family: sans-serif;">\n');
    response.write('<div style="opacity: .2; z-index: -1; width: 100%; height: 10em; position: absolute; background: url(https://s3.amazonaws.com/technicalmachine-assets/technical-io/tessel-name.png) no-repeat scroll 0 0 / contain"></div>\n');

    response.write('<div style="padding: 2em;">');
  },
  writeFooter: function( response ) {
    response.write("</div>");
    response.write('<code style="font: sans-serif; float: right; padding: 3em;">');
    response.write('powered by <a href="https://tessel.io/">tessel</a>');
    response.write(' (<a href="http://git.gametheorylabs.com/tessel-static-webcam">sms code</a>)');
    response.write(' (<a href="http://git.gametheorylabs.com/tessel-static-webcam">camera code</a>)</code>');
    response.end("</body><html>\n");
  }
};




